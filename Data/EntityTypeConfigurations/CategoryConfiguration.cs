﻿using Data.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.EntityTypeConfigurations
{
    class CategoryConfiguration : EntityTypeConfiguration<Category>
    {
        public CategoryConfiguration()
        {
            HasKey(c => c.Id);

            Property(c => c.Name)
                .IsRequired()
                .HasMaxLength(255);

            HasMany(c => c.Posts)
                .WithMany(p => p.Categories)
                .Map(m => 
                {
                    m.MapLeftKey("CategoryId");
                    m.MapRightKey("PostId");
                    m.ToTable("CategoryPosts");
                });
                
        }
    }
}
