﻿using Data.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.EntityTypeConfigurations
{
    public class PostConfiguration : EntityTypeConfiguration<Post>
    {
        public PostConfiguration()
        {
            Property(p => p.Content)
                .IsRequired()
                .IsMaxLength();

            Property(p => p.Title)
                .IsRequired()
                .HasMaxLength(300);
        }
    }
}
