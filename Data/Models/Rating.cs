﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Models
{
    public enum Rating
    {
        Bad = 0,
        NotBad = 1,
        Normal = 2,
        Good = 3,
        Cool = 4
    }
}
