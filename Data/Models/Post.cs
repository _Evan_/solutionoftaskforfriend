﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Models
{
    public class Post
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public Author Author { get; set; }
        public int AuthorId { get; set; }
        public Rating Rating { get; set; }
        public ICollection<Category> Categories { get; set; }

        public Post()
        {
            Categories = new HashSet<Category>();
        }
    }
}
