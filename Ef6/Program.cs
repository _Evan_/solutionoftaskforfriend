﻿using System;
using Data;
using Data.Models;

namespace Ef6
{
    class Program
    {
        static void Main(string[] args)
        {

            using (var unitOfWork = new UnitOfWork(new BlogContext()))
            {
                unitOfWork.Authors.Add(new Author()
                {
                    Name = "FirstName",
                    Surname = "SecondSurname",
                });

                foreach (var author in unitOfWork.Authors.GetAll())
                {
                    Console.WriteLine($"Name: {author.Name} Surname: {author.Surname}");
                }

                var auth = unitOfWork.Authors.Find(a => a.Name.Contains("FirstName"));

                unitOfWork.Authors.RemoveRange(auth);

                unitOfWork.Save();
            }

            Console.ReadKey();
        }
    }
}
